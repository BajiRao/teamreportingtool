package com.grey.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.grey.connections.PostgresConnection;
@WebServlet(urlPatterns="/signup.do")
public class SignUpServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PostgresConnection database=new PostgresConnection();
		ServletContext con =getServletContext();
		
		String username=req.getParameter("uname");
		String password=req.getParameter("password");
		String email=req.getParameter("email");
		String securityQuestion=req.getParameter("securityQuestion");
		String securityAnswer=req.getParameter("securityAnswer");
		try {
			if(database.createUser(username,password,email,securityQuestion,securityAnswer)>0) {
				req.setAttribute("successMessage", "Account Created, Login Now");
				resp.setStatus(201);
				con.log("Account created for user "+username);
				req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, resp);
			}else
			{
				req.setAttribute("errorMessage", "Failed Please Try Again");
				con.log("Account could not be creaeted");
				req.getRequestDispatcher("/WEB-INF/views/SignUp.jsp").forward(req, resp);
			}
		} catch (ClassNotFoundException | SQLException e) {
			req.setAttribute("errorMessage", e.getMessage());
			con.log(e.getMessage(),e);
			e.printStackTrace();
		}
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/views/SignUp.jsp").forward(req, resp);
	}
}
