package com.grey.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.grey.connections.PostgresConnection;
import com.grey.utils.ActivityClass;

@WebServlet(urlPatterns = "/MyActivity.do")
public class MyActivity extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		ServletContext con = getServletContext();
		ResultSet rs = null;
		String option = req.getParameter("option");
		System.out.println(option);
		String from = req.getParameter("from")==null?"":(String) req.getParameter("from");
		String to = req.getParameter("to")==null?"":(String) req.getParameter("to");
		PostgresConnection database = new PostgresConnection();
		LinkedList<ActivityClass> activities = new LinkedList<>();
		ActivityClass activity = new ActivityClass();
		if (session.getAttribute("uname") == null) {
			resp.setStatus(401);
			con.log("Session has Expired");
			req.setAttribute("errorMessage", "Session Expired, Please Log In Again!");
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
			return;
		}
		if(option==null) {
			con.log("No Option Provided To Fetch Avtivity");
			req.getRequestDispatcher("/WEB-INF/views/MyActivity.jsp").forward(req, resp);
			return;
		}
		try {
			System.out.println("" + (int) session.getAttribute("id"));
			rs = database.getMyActivity((int) session.getAttribute("id"), option, from, to);
			while (rs.next()) {
				activity = new ActivityClass();
				activity.setId(rs.getInt(1));
				activity.setSubject(rs.getString(2));
				activity.setHeader(rs.getString(3));
				activity.setDate(rs.getDate(4).toString());
				activity.setLastupdated(rs.getDate(5).toString());
				activity.setPriority(rs.getString(6));
				activity.setUsername(rs.getString(8));
				activities.add(activity);
			}
			req.setAttribute("activities", activities);
			con.log("Activity Data has been fetched for " + session.getAttribute("uname"));
			req.getRequestDispatcher("/WEB-INF/views/MyActivity.jsp").forward(req, resp);
		} catch (ClassNotFoundException | SQLException e) {
			con.log(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
