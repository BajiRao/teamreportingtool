<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<style>
div#header {
	position: sticky;
	height: 40px;
	font-family: verdana;
}
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${sessionScope.uname} logged in</title>
</head>
<body>
	<header title="DashBoard">
	<form action="logout.do" method="post">
		<div id="header" align="right">
			<input type="submit" value="logout">
		</div>
	</form>
	</header>
	<div align="center">
		<p>
			<font color="green">${successMessage}</font>
		</p>
	</div>
	<form action="logout.do" method="post">
		<h1>
			<b>Welcome ${uname} your id is ${id} and your role is</b>
			<c:choose>
				<c:when test="${role==1}">
					<b>Developer</b>
				</c:when>
				<c:when test="${role==2}">
					<b>Team Lead</b>
				</c:when>
				<c:when test="${role==3}">
					<b>Manager/Admin</b>
				</c:when>
			</c:choose>
		</h1>
		<a href="MyActivity.do">View my activity</a><br>
		<a href="AddActivity.do">Add an activity</a><br> 
		<a	href="UpdateActivity.do">Update An Activity</a>
		<c:choose><c:when test="${role>=2}"><a href="MyResources.do">My team activity</a></c:when></c:choose>
		
	</form>

</body>
</html>