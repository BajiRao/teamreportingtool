package com.grey.servlets;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns="/DashBoard.do")
public class DashBoard extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		ServletContext con=getServletContext();
		if (session.getAttribute("uname") == null) {
			resp.setStatus(401);
			req.setAttribute("errorMessage", "Session Expired, Please Log In Again!");
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
			return;
		}
		req.getRequestDispatcher("/WEB-INF/views/Welcome.jsp").forward(req, resp);
		con.log("Welcome to Dashboard "+session.getAttribute("uname"));
	}
}
