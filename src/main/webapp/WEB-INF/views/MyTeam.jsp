<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 10px;
	text-align: center;
}

table#t01 {
	width: 90%;
	background-color: #f1e1d1;
	padding: 16px;
}

div#table {
	border: 3px solid #f1f1f1;
	width: 100%;
}

div#searchFilters {
	position: sticky;
	border: 3px solid #f1f1f1;
	font-family: verdana;
}

div#header {
	position: sticky;
	height: 40px;
	font-family: verdana;
	width: 30%;
}

input[type=submit], input[type=button] {
	top: 12px;
}
</style>
<script type="text/javascript">
function exportTableToExcel(tableID, filename = ''){ var downloadLink;
var dataType = 'application/vnd.ms-excel'; var tableSelect =
document.getElementById(tableID); var tableHTML =
tableSelect.outerHTML.replace(/ /g, '%20'); filename =
filename?filename+'.xls':'excel_data.xls'; downloadLink =
document.createElement("a"); document.body.appendChild(downloadLink);
if(navigator.msSaveOrOpenBlob){ var blob = new Blob(['\ufeff',
tableHTML], { type: dataType }); navigator.msSaveOrOpenBlob( blob,
filename); }else{ downloadLink.href = 'data:' + dataType + ', ' +
tableHTML;downloadLink.download = filename; downloadLink.click(); } }
</script>
<head>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
	$(function() {
		$("#datepicker1").datepicker();
	});
</script>
<script type="text/javascript">
function generateRequiredElements(){
	var a=document.getElementById('op').value;
	var from=document.getElementById('from');
	var to=document.getElementById('to');
	if(a=="In Between"){
		if(from.style.display==="none")
			from.style.display="block";
		if(to.style.display==="none")
			to.style.display="block";
		
	}else if(a=='Of Date'){
		if(from.style.display==="none")
			from.style.display="block";
		to.style.display="none"
	}
	else{
		from.style.display="none"
			to.style.display="none"
	}
};
function persist(){
	//if('${option}'==null)
		alert("${option}");
}

</script>
<title>${sessionScope.uname}'sActivity</title>
</head>

<body onLoad="persist()">
	<header title="Activity">
		<form action="DashBoard.do" method="post">
			<div id="header" align="left">
				<input type="button" value="Export"
					onclick="exportTableToExcel('t01')"> <input type="submit"
					value="Dashboard">
			</div>
		</form>
		<div id="searchFilters" align="right">
			<form action="MyResources.do" method="post" novalidate>
				<select name="option" id="op"
					onChange="generateRequiredElements()"><option
						selected="selected">All</option>
					<option>Last 30 Days</option>
					<option>In Between</option>
					<option>Of Date</option></select>
				<div align="right">
					<div id="from" style="display: none;">
						<label for="date"><b>Date</b></label> <input type="text"
							name="from" id="datepicker" required>
					</div>
					<div id="to" style="display: none;">
						<label for="date"><b>To</b></label> <input type="text" name="to"
							id="datepicker1" required>
					</div>
				</div>
				<button type="submit"> View </button>
			</form>
		</div>
	</header>
	<h1><font color="green">Your Activity</font></h1>
	<div id="table">
		<table id="t01">
			<tr>
				<th>User Id</th>
				<th>username</th>
				<th>Activity</th>
				<th>Description</th>
				<th>Date</th>
				<th>Last Modified</th>
				<th>Priority</th>
			</tr>
			<c:forEach var="ac" items="${requestScope.activities}">
				<tr>
					<td><c:out value="${ac.id}"></c:out></td>
					<td><c:out value="${ac.username}"></c:out></td>
					<td><c:out value="${ac.subject}"></c:out></td>
					<td><c:out value="${ac.header}"></c:out></td>
					<td><c:out value="${ac.date}"></c:out></td>
					<td><c:out value="${ac.lastupdated}"></c:out></td>
					<td><c:out value="${ac.priority}"></c:out></td>
				</tr>
			</c:forEach>
		</table>

	</div>


</body>
</html>