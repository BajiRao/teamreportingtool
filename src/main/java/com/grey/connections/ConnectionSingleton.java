package com.grey.connections;

import java.sql.Connection;
import java.util.Properties;

public class ConnectionSingleton {
	private static Connection connection;
	private static Properties properties=new Properties();
	public static Connection getConnection() {
		return connection;
	}
	public static void setConnection(Connection connection) {
		ConnectionSingleton.connection = connection;
	}
	public static Properties getProperties() {
		return properties;
	}
	public static void setProperties(Properties properties) {
		ConnectionSingleton.properties = properties;
	}
}
