package com.grey.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.grey.connections.PostgresConnection;

@WebServlet(urlPatterns = "/AddActivity.do")
public class AddActivity extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		ServletContext con=getServletContext();
		PostgresConnection database=new PostgresConnection();
		if (session.getAttribute("uname") == null) {
			resp.setStatus(401);
			req.setAttribute("errorMessage", "Session Expired, Please Log In Again!");
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
			con.log("Un Authorized: session Expired",new ServletException("Request Timed Out"));
			return;
		}
		if (req.getParameter("subject") == null) {
			req.getRequestDispatcher("/WEB-INF/views/AddActivity.jsp").forward(req, resp);
			con.log("Add Activity Request found");
			return;
		}
		int id = (Integer) session.getAttribute("id");
		String subject = (String) req.getParameter("subject");
		String description = (String) req.getParameter("description");
		String priority = (String) req.getParameter("priority");
		String Date = (String) req.getParameter("date");
		try {
			database.createActivity(id, subject, description, Date, priority);
			con.log("201 Activity has been created for "+id);
			req.setAttribute("successMessage", "Activity has been added");
			req.getRequestDispatcher("/WEB-INF/views/AddActivity.jsp").forward(req, resp);
		} catch (ClassNotFoundException e) {
			con.log(e.getMessage(),e);
		} catch (SQLException e) {
			req.setAttribute("errorMessage", e.getMessage());
			req.getRequestDispatcher("/WEB-INF/views/AddActivity.jsp").forward(req, resp);
			con.log(e.getMessage(),e);
		}
		
	} 	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
