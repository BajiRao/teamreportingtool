<%@ page isELIgnored="false" import="java.net.URI"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

form {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
}

button:hover {
	opacity: 0.8;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}

img.avatar {
	width: 40%;
	border-radius: 50%;
}

.container {
	padding: 16px;
}

.formcontainer {
	width: 90%;
	text-align: left;
	padding: 16px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
</style>
<body>
	<form action="login.do" method="post" class="formcontainer">
		<div class="imgcontainer">
			<img src="/resources/images/img_avatar2.png" alt="Avatar"
				class="avatar">
		</div>

		<div align="center">
			<p>
				<font color="red">${errorMessage}</font>
			</p>
		</div>

		<div class="container">
			<label for="uname"><b>Username</b></label> <input type="text"
				placeholder="Enter Username" name="uname" required> <label
				for="password"><b>Password</b></label> <input type="password"
				placeholder="Enter Password" name="password" required>

			<button type="submit">Login</button>
		</div>

		<div class="container" style="background-color: #f1f1f1">
			<button type="reset" class="cancelbtn">Clear</button>
			<span class="psw"><a href="#">Forgot password?</a></span>
			<div class="psw" style="background-color: #f1f1f1">
				<a href="signup.do">Sign Up</a>
			</div>
		</div>
	</form>
</body>
</html>
