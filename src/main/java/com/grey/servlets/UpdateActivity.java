package com.grey.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.grey.connections.PostgresConnection;

@WebServlet(urlPatterns = "/UpdateActivity.do")
public class UpdateActivity extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		ServletContext con = getServletContext();
		ResultSet rs = null;

		if (session.getAttribute("uname") == null) {
			resp.setStatus(401);
			req.setAttribute("errorMessage", "Session Expired, Please Log In Again!");
			con.log("Session has Expired");
			req.getRequestDispatcher("/index.jsp").forward(req, resp);
			return;
		}
		String Date = (String) req.getParameter("date");
		String username=(String) session.getAttribute("uname");
		String subject = (String) req.getParameter("subject");
		String description = (String) req.getParameter("description");
		String priority = (String) req.getParameter("priority");
		int id = (int) session.getAttribute("id");
		if (Date == null) {
			req.getRequestDispatcher("/WEB-INF/views/UpdateActivity.jsp").forward(req, resp);
			return;
		} else if (subject != null) {
			try {
				if(new PostgresConnection().updateUserActivity(id, Date, subject, description, priority)>=1) {
					req.setAttribute("successMessage", "Activity has been updated for "+username+" of Date"+Date);
					req.getRequestDispatcher("/WEB-INF/views/Welcome.jsp").forward(req, resp);
					return;
				}
			}
			catch (Exception e) {
				con.log(e.getMessage(),e);
			}
		} else {
			try {
				rs = new PostgresConnection().getUpdateActivity(id, Date);
			} catch (ClassNotFoundException | SQLException e) {
				con.log(e.getMessage(), e);
			}
			try {
				while (rs.next()) {
					req.setAttribute("subject", rs.getString(2));
					req.setAttribute("description", rs.getString(3));
					req.setAttribute("addeddate", rs.getDate(4).toString());
					req.setAttribute("priority", rs.getString(6));
					req.setAttribute("date", Date);
				}
				con.log("Data fetched for update " + Date + " of user " + session.getAttribute("uname"));
				req.getRequestDispatcher("/WEB-INF/views/UpdateActivity.jsp").forward(req, resp);
			} catch (SQLException e) {
				con.log(e.getMessage(), e);
			}

		}

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
