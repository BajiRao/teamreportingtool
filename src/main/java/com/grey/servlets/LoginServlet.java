package com.grey.servlets;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.grey.connections.PostgresConnection;
import com.grey.utils.AuthenticationUtils;

@WebServlet(urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpSession session = null;
	private ResultSet rs = null;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		PostgresConnection database=new PostgresConnection();
		String uname = req.getParameter("uname");
		ServletContext con=getServletContext();
		String pwd = req.getParameter("password");
		if (uname == null || pwd == null) {
			req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, res);
			return;
		}
		try {
			if (req.getSession().getAttribute("uname") != null) {
				req.getRequestDispatcher("/WEB-INF/views/Welcome.jsp").forward(req, res);
				return;
			}
			if (AuthenticationUtils.authenticate(uname, pwd)) {
				req.setAttribute(uname, "uname");
				res.setStatus(200);
				session = req.getSession(true);
				session.setMaxInactiveInterval(3 * 60);
				;
				rs = database.getDetails(uname);
				while (rs.next()) {
					session.setAttribute("uname", uname);
					con.log(uname+" logged in successfully");
					req.setAttribute("successMessage", "Welcome, "+uname+" you have successfully logged in");
					session.setAttribute("role", rs.getInt(4));
					session.setAttribute("id", rs.getInt(1));
				}
				req.getRequestDispatcher("/WEB-INF/views/Welcome.jsp").forward(req, res);
			} else {
				req.setAttribute("errorMessage", "Failed Log in please try again");
				res.setStatus(401);
				req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, res);
			}
		} catch (Exception e) {
			e.printStackTrace();
			req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, res);
		}

	}

	// @Override
	// protected void doGet(HttpServletRequest req, HttpServletResponse res) throws
	// IOException, ServletException {
	// doPost(req, res);
	// }

}