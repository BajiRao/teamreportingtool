-- FUNCTION: public.beforeinsert()

-- DROP FUNCTION public.beforeinsert();

CREATE FUNCTION public.beforeinsert()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$ declare 
role1 int;
begin 
select user_role into role1 from users where pk_id=new.mgrtl_id;
IF  role1 > 2 and new.mgrtl_id<> new.e_id THEN
  RETURN NEW;
ELSE 
  RAISE EXCEPTION 'ERROR: Not Allowed';
END IF;
End;$BODY$;

ALTER FUNCTION public.beforeinsert()
    OWNER TO postgres;




-- SEQUENCE: public.inc

-- DROP SEQUENCE public.inc;

CREATE SEQUENCE public.inc
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.inc
    OWNER TO postgres;

-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    pk_id integer NOT NULL DEFAULT nextval('inc'::regclass),
    username character varying(32) COLLATE pg_catalog."default",
    password character varying(256) COLLATE pg_catalog."default",
    user_role integer DEFAULT 1,
    email_address character varying(128) COLLATE pg_catalog."default",
    security_question character varying(128) COLLATE pg_catalog."default",
    security_answer character varying(256) COLLATE pg_catalog."default",
    CONSTRAINT users_pkey PRIMARY KEY (pk_id),
    CONSTRAINT users_username_key UNIQUE (username)

)

TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;


-- Table: public.employeemanagermappings

-- DROP TABLE public.employeemanagermappings;

CREATE TABLE public.employeemanagermappings
(
    mgrtl_id integer,
    e_id integer,
    CONSTRAINT employeemanagermappings_e_id_fkey FOREIGN KEY (e_id)
        REFERENCES public.users (pk_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT employeemanagermappings_mgrtl_id_fkey FOREIGN KEY (mgrtl_id)
        REFERENCES public.users (pk_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

-- Table: public.useractivities

-- DROP TABLE public.useractivities;

CREATE TABLE public.useractivities
(
    fk_id integer,
    activityheader character varying(128) COLLATE pg_catalog."default",
    activitydescription character varying(512) COLLATE pg_catalog."default",
    addeddate timestamp without time zone,
    lastupdateddate timestamp without time zone DEFAULT now(),
    priority character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT workdate UNIQUE (addeddate, fk_id)
,
    CONSTRAINT useractivities_fk_id_fkey FOREIGN KEY (fk_id)
        REFERENCES public.users (pk_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT useractivities_priority_check CHECK (priority::text = 'Normal'::text OR priority::text = 'Medium'::text OR priority::text = 'High'::text)
)

TABLESPACE pg_default;

ALTER TABLE public.useractivities
    OWNER to postgres;

TABLESPACE pg_default;

ALTER TABLE public.employeemanagermappings
    OWNER to postgres;

-- Trigger: t1

-- DROP TRIGGER t1 ON public.employeemanagermappings;

CREATE TRIGGER t1
    BEFORE INSERT
    ON public.employeemanagermappings
    FOR EACH ROW
    EXECUTE PROCEDURE public.beforeinsert();