<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

form[id=f1] {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password], select {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
}

button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
}

button:hover {
	opacity: 0.8;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}

img.avatar {
	width: 40%;
	border-radius: 50%;
}

.container {
	padding: 16px;
}

.formcontainer {
	width: 70%;
	text-align: left;
	padding: 16px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
div#header {
	position: sticky;
	height: 40px;
	font-family: verdana;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
<title>Add Activity</title>
</head>
<body>
	<header title="Activity">
	<form action="DashBoard.do" method="post">
		<div id="header" align="right">
			<input type="submit" value="Dashboard">
		</div>
	</form>
	</header>
	<h1>Welcome ${uname}</h1>
	<div align="center">
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	</div>
	<div align="center">
		<p>
			<font color="green">${successMessage}</font>
		</p>
	</div>
	<form id="f1" action="AddActivity.do" method="post">
		<div class="container">
			<label for="subject"><b>Subject</b></label> <input type="text"
				placeholder="subject" name="subject" required> <label
				for="description"><b>Description</b></label> <input type="text"
				placeholder="description" name="description" required> <label
				for="date"><b>Date</b></label> <input type="text" name="date"
				id="datepicker" required> <select name="priority"><option
					selected="selected">Normal</option>
				<option>Medium</option>
				<option>High</option></select>
			<button type="submit">Add</button>
		</div>
	</form>

</body>
</html>