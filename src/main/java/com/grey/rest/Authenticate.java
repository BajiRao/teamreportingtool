package com.grey.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.grey.utils.AuthenticationUtils;

@Path("/auth")
public class Authenticate {
	@Context
	private HttpServletRequest request;
	private static Logger logger = LogManager.getLogger(Authenticate.class);
	@POST
	@Path("/auth")
	@Produces("text/html")
	public Response getSession(@QueryParam("username") String username, @QueryParam("password") String password) throws Exception {
		if (AuthenticationUtils.authenticate(username, password)) {
			request.getSession(true);
			return Response.status(200).entity("OK").build();
			
		}
		return Response.status(401).entity("Authentication Error").build();
	}
}
