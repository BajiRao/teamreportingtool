package com.grey.connections;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.grey.utils.AuthenticationUtils;

public class PostgresConnection {

	private File getFileFromResources(String fileName) {

		ClassLoader classLoader = getClass().getClassLoader();

		URL resource = classLoader.getResource(fileName);
		if (resource == null) {
			throw new IllegalArgumentException("file not found!");
		} else {
			return new File(resource.getFile());
		}

	}

	public void init() throws FileNotFoundException, IOException {
		Properties properties = ConnectionSingleton.getProperties();
		properties.load(new FileInputStream(new PostgresConnection().getFileFromResources("database.properties")));
		ConnectionSingleton.setProperties(properties);
	}

	public void createConnection() throws SQLException, FileNotFoundException, IOException, ClassNotFoundException {
		Properties properties = new Properties();
		if (ConnectionSingleton.getProperties().isEmpty()) {
			init();
			properties = ConnectionSingleton.getProperties();
		}
		properties = ConnectionSingleton.getProperties();
		Connection connection = ConnectionSingleton.getConnection();
		Class.forName("org.postgresql.Driver");
		connection = DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("username"),
				properties.getProperty("password"));
		ConnectionSingleton.setConnection(connection);
	}

	public ResultSet getDetails(String username)
			throws SQLException, FileNotFoundException, IOException, ClassNotFoundException {
		Connection connection = null;
		if (ConnectionSingleton.getConnection() == null) {
			createConnection();
			connection = ConnectionSingleton.getConnection();
		}
		connection = ConnectionSingleton.getConnection();
		PreparedStatement pstmt = connection.prepareStatement("select * from users where username=?");
		pstmt.setString(1, username);
		return pstmt.executeQuery();
	}

	public ResultSet getTeamDetails(int id, String option, String resourceName, String from, String to)
			throws SQLException, FileNotFoundException, IOException, ClassNotFoundException {
		Connection connection = null;
		if (ConnectionSingleton.getConnection() == null) {
			createConnection();
			connection = ConnectionSingleton.getConnection();
		}
		connection = ConnectionSingleton.getConnection();
		connection = ConnectionSingleton.getConnection();
		if (option.equals("Last 30 Days"))
			return connection.prepareStatement(
					"select * from useractivities, users where fk_id=pk_id and Date_Part('day',now() - addeddate) < 30 and fk_id="
							+ id + " order by addeddate asc")
					.executeQuery();
		if (option.equals("In Between"))
			return connection
					.prepareStatement("select * from useractivities, users where fk_id=pk_id and addeddate between '"
							+ from + "' and '" + to + "'and fk_id=" + id + " order by addeddate asc")
					.executeQuery();
		if (option.equals("Of Date"))
			return connection.prepareStatement("select * from useractivities, users where fk_id=pk_id and addeddate='"
					+ from + "' and fk_id=" + id + " order by addeddate asc").executeQuery();
		return connection.prepareStatement(
				"select fk_id, activityheader, activitydescription, addeddate, lastupdateddate, priority, username from useractivities,users,employeemanagermappings where fk_id=e_id and pk_id=e_id and mgrtl_id="
						+ id + " order by addeddate asc").executeQuery();
		
	}

	public int createUser(String username, String password, String email, String securityQuestion,
			String securityAnswer) throws FileNotFoundException, ClassNotFoundException, SQLException, IOException {
		Connection connection = null;
		if (ConnectionSingleton.getConnection() == null) {
			createConnection();
			connection = ConnectionSingleton.getConnection();
		}
		connection = ConnectionSingleton.getConnection();
		return connection.prepareStatement(
				"insert into users(\"username\",\"password\",\"email_address\",\"security_question\",\"security_answer\") values(\'"
						+ username + "\',\'" + AuthenticationUtils.encrypt(password) + "\',\'" + email + "\',\'"
						+ securityQuestion + "\',\'" + AuthenticationUtils.encrypt(securityAnswer) + "\')")
				.executeUpdate();
	}

	public int createActivity(int userId, String subject, String description, String date, String priority)
			throws FileNotFoundException, ClassNotFoundException, SQLException, IOException {
		Connection connection = null;
		if (ConnectionSingleton.getConnection() == null) {
			createConnection();
			connection = ConnectionSingleton.getConnection();
		}
		connection = ConnectionSingleton.getConnection();

		return connection.prepareStatement(
				"INSERT INTO public.useractivities(fk_id, activityheader, activitydescription, addeddate, priority) VALUES ("
						+ userId + ", '" + subject + "','" + description + "', TO_Date('" + date + "', 'MM/DD/YYYY'),'"
						+ priority + "')")
				.executeUpdate();
	}

	public ResultSet getMyActivity(int id, String option, String from, String to)
			throws SQLException, FileNotFoundException, ClassNotFoundException, IOException {
		Connection connection = null;
		if (ConnectionSingleton.getConnection() == null) {
			createConnection();
			connection = ConnectionSingleton.getConnection();
		}
		connection = ConnectionSingleton.getConnection();
		if (option.equals("Last 30 Days"))
			return connection.prepareStatement(
					"select * from useractivities, users where fk_id=pk_id and Date_Part('day',now() - addeddate) < 30 and fk_id="
							+ id + " order by addeddate asc")
					.executeQuery();
		if (option.equals("In Between"))
			return connection
					.prepareStatement("select * from useractivities, users where fk_id=pk_id and addeddate between '"
							+ from + "' and '" + to + "'and fk_id=" + id + " order by addeddate asc")
					.executeQuery();
		if (option.equals("Of Date"))
			return connection.prepareStatement("select * from useractivities, users where fk_id=pk_id and addeddate='"
					+ from + "' and fk_id=" + id + " order by addeddate asc").executeQuery();
		return connection.prepareStatement(
				"select * from useractivities, users where fk_id=pk_id and fk_id=" + id + " order by addeddate asc")
				.executeQuery();
	}

	public ResultSet getUpdateActivity(int id, String date)
			throws SQLException, FileNotFoundException, ClassNotFoundException, IOException {
		Connection connection = null;
		if (ConnectionSingleton.getConnection() == null) {
			createConnection();
			connection = ConnectionSingleton.getConnection();
		}
		connection = ConnectionSingleton.getConnection();

		return connection
				.prepareStatement("SELECT * FROM public.useractivities where addeddate='" + date + "' and fk_id=" + id)
				.executeQuery();
	}

	public int updateUserActivity(int id, String date, String subject, String description, String priority)
			throws SQLException, FileNotFoundException, ClassNotFoundException, IOException {
		Connection connection = null;
		if (ConnectionSingleton.getConnection() == null) {
			createConnection();
			connection = ConnectionSingleton.getConnection();
		}
		connection = ConnectionSingleton.getConnection();
		return connection.prepareStatement("update public.useractivities SET fk_id=" + id + ", activityheader='"
				+ subject + "', activitydescription='" + description + "', addeddate=To_date('" + date
				+ "', 'MM/DD/YYYY'), lastupdateddate=now(), priority='" + priority + "' where addeddate='" + date
				+ "' and fk_id=" + id).executeUpdate();
	}

}
